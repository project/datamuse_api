<?php

/**
 * @file
 * Constants for Datamuse API module.
 */

define('DATAMUSE_API_PATH', 'https://api.datamuse.com');

// Means like constraint: require that the results have a meaning related
// to this string value, which can be any word or sequence of words.
// (This is effectively the reverse dictionary feature of OneLook.)
define('DATAMUSE_API_WORDS_MEAN_LIKE', 'ml');

// Sounds like constraint: require that the results are pronounced similarly
// to this string of characters. (If the string of characters doesn't have a
// known pronunciation, the system will make its best guess
// using a text-to-phonemes algorithm.)
define('DATAMUSE_API_WORDS_SOUNDS_LIKE', 'sl');

// Spelled like constraint: require that the results are spelled similarly
// to this string of characters, or that they match this wildcard pattern.
// A pattern can include any combination of alphanumeric characters, spaces,
// and two reserved characters that represent placeholders — * (which matches
// any number of characters) and ? (which matches exactly one character).
define('DATAMUSE_API_WORDS_SPELLED_LIKE', 'sp');

// Topic words: An optional hint to the system about the theme of the document
// being written. Results will be skewed toward these topics.
//At most 5 words can be specified. Space or comma delimited. Nouns work best.
define('DATAMUSE_API_WORDS_TOPIC_WORDS', 'topics');

// Left context: An optional hint to the system about the word that appears
// immediately to the left of the target word in a sentence.
//(At this time, only a single word may be specified.)
define('DATAMUSE_API_WORDS_LEFT_CONTEXT', 'lc');

// Right context: An optional hint to the system about the word that appears
// immediately to the right of the target word in a sentence.
//(At this time, only a single word may be specified.)
define('DATAMUSE_API_WORDS_RIGHT_CONTEXT', 'rc');

// Related word constraints: require that each of the resulting words,
// when paired with the word in this parameter, are in a predefined lexical
// relation indicated by [code]. Any number of these parameters may be specified
// any number of times. An assortment of semantic, phonetic,
// and corpus-statistics-based relations are available.
define('DATAMUSE_API_WORDS_RELATED_WORD', 'rec_');

// Popular nouns modified by the given adjective, per Google Books Ngrams.
define('DATAMUSE_API_WORDS_RELATED_WORD_JJA', 'jja');
// Popular adjectives used to modify the given noun, per Google Books Ngrams.
define('DATAMUSE_API_WORDS_RELATED_WORD_JJB', 'jjb');
// Synonyms (words contained within the same WordNet synset).
define('DATAMUSE_API_WORDS_RELATED_WORD_SYN', 'syn');
// Antonyms (per WordNet).
define('DATAMUSE_API_WORDS_RELATED_WORD_ANT', 'ant');
// "Kind of" (direct hypernyms, per WordNet).
define('DATAMUSE_API_WORDS_RELATED_WORD_SPC', 'spc');
// "More general than" (direct hyponyms, per WordNet).
define('DATAMUSE_API_WORDS_RELATED_WORD_GEN', 'gen');
// "Comprises" (direct holonyms, per WordNet).
define('DATAMUSE_API_WORDS_RELATED_WORD_COM', 'com');
// "Part of" (direct meronyms, per WordNet).
define('DATAMUSE_API_WORDS_RELATED_WORD_PAR', 'par');
// Frequent followers (w′ such that P(w′|w) ≥ 0.001 per Google Books Ngrams).
define('DATAMUSE_API_WORDS_RELATED_WORD_BGA', 'bga');
// Frequent predecessors (w′ such that P(w|w′) ≥ 0.001 per Google Books Ngrams).
define('DATAMUSE_API_WORDS_RELATED_WORD_BGB', 'bgb');
// Rhymes ("perfect" rhymes, per RhymeZone)
define('DATAMUSE_API_WORDS_RELATED_WORD_RHY', 'rhy');
// Approximate rhymes (per RhymeZone).
define('DATAMUSE_API_WORDS_RELATED_WORD_NRY', 'nry');
// Homophones (sound-alike words).
define('DATAMUSE_API_WORDS_RELATED_WORD_HOM', 'hom');
// Consonant match.
define('DATAMUSE_API_WORDS_RELATED_WORD_CSN', 'cns');
